
var request = require("request");
var bodyParser = require("body-parser");




// Server index page
exports.handler = function(event,context) {
  getPosts(context);
}

function buildResponse(options) {

  if(process.env.NODE_DEBUG_EN) {
    console.log("buildResponse options:\n"+JSON.stringify(options,null,2));
  }

  var response = {
    version: "1.0",
    response: {
      outputSpeech: {
        type: "SSML",
        ssml: "<speak>"+options.speechText+"</speak>"
      },
      shouldEndSession: options.endSession
    }
  };

  if(options.repromptText) {
    response.response.reprompt = {
      outputSpeech: {
        type: "SSML",
        ssml: "<speak>"+options.repromptText+"</speak>"
      }
    };
  }

  if(options.cardTitle) {
    response.response.card = {
      type: "Simple",
      title: options.cardTitle
    }

    if(options.imageUrl) {
      response.response.card.type = "Standard";
      response.response.card.text = options.cardContent;
      response.response.card.image = {
        smallImageUrl: options.imageUrl,
        largeImageUrl: options.imageUrl
      };

    } else {
      response.response.card.content = options.cardContent;
    }
  }


  if(options.session && options.session.attributes) {
    response.sessionAttributes = options.session.attributes;
  }

  if(process.env.NODE_DEBUG_EN) {
    console.log("Response:\n"+JSON.stringify(response,null,2));
  }

  return response;
}


function getPosts(context){
  console.log("invoked getPosts");


  request({
      url: "https://graph.facebook.com/oauth/access_token?client_id="+process.env.CLIENT_ID+"&client_secret="+process.env.CLIENT_SECRET+"&grant_type=client_credentials",     
      method: "GET"
    }, function(err, res, token) {
      
      if (err) {
        console.log(err);
        context.fail(err);
       
      } else {
        let accessToken = JSON.parse(token).access_token;
        connectWeatherMan(context,accessToken);
      }      
    });
}

function connectWeatherMan(context,accessToken)
{
  request({
    url: "https://graph.facebook.com/v2.9/tamilnaduweatherman/posts",
    qs: {
      access_token: accessToken       
    },
    method: "GET"
  }, function(error, response, body) {
    
    if (error) {
      if(process.env.NODE_DEBUG_EN) {
        console.log("Error : " +  error);
      }
      context.fail(error);
    } else {
      if(process.env.NODE_DEBUG_EN) {
        console.log("body :"+body);
      }
      let options = {};
      
      
      var msgCreatedTime = Date.parse(JSON.parse(body).data[0].created_time);

      let currentTime = new Date().getTime();
      // get total seconds between the times

      if(process.env.NODE_DEBUG_EN) {
        console.log("current time :"+currentTime);
        console.log("Post time :"+msgCreatedTime);
      }
      var delta = Math.abs(currentTime - msgCreatedTime) / 1000;

      // calculate (and subtract) whole days
      var days = Math.floor(delta / 86400);
      delta -= days * 86400;

      // calculate (and subtract) whole hours
      var hours = Math.floor(delta / 3600) % 24;
      delta -= hours * 3600;

      // calculate (and subtract) whole minutes
      var minutes = Math.floor(delta / 60) % 60;
      delta -= minutes * 60;

      // what's left is seconds
      var seconds = delta % 60; 

      if(process.env.NODE_DEBUG_EN) {
        console.log("Last Post was before :"+days+" days"+hours+" hours"+minutes+" minutes"+seconds+" seconds");
      }


      let speechText = "The last message by Tamilnadu Weatherman was posted ";
      if(days===1) speechText += days+" day ago";
      else if(days) speechText += days+" days ago";
      else if(hours===1) speechText += hours+" hour ago";
      else if(hours) speechText += hours+" hours ago";
      else if(minutes===1) speechText += minutes+" minute ago";
      else if(minutes) speechText += minutes+" minutes ago";
      else if(seconds===1) speechText += seconds+" second ago";
      else if(seconds) speechText += seconds+" seconds ago";

      speechText += " with the following message.\n ";
      speechText  += JSON.parse(body).data[0].message;
      
      var desiredText = speechText.replace(/[^a-zA-Z0-9'"%&!@$.,;:\-\n/ ]/g, "");
      var trimmedDesiredText = "";
      if(desiredText.length>2500){
        let sentences = desiredText.split(".");
        sentences.forEach(function(value){
          if(trimmedDesiredText.length<7600)
            trimmedDesiredText = trimmedDesiredText+value+". ";          
        });
        desiredText = trimmedDesiredText;
      }

      var textToAppend = "Please check the facebook page of Tamilnadu Weatherman for more details.";
      var desiredText = desiredText+textToAppend;
      
      if(process.env.NODE_DEBUG_EN) {
        console.log("Text being Delivered :"+desiredText);
      }
      
      options.speechText = desiredText;
      options.endSession = true;
      context.succeed(buildResponse(options));
    }      
  });
}